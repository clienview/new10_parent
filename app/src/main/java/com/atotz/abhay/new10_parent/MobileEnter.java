package com.atotz.abhay.new10_parent;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import com.atotz.abhay.new10_parent.adapters.MobileEnterSliderAdapter;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.Timer;
import java.util.TimerTask;

public class MobileEnter extends AppCompatActivity {
EditText mobile;
CardView btn;
MobileEnterSliderAdapter mobileEnterSliderAdapter;
ViewPager viewPager;
int currentPage=0,NUM_PAGES=3;
    Handler handler = new Handler();
    Runnable update;
    AVLoadingIndicatorView avLoadingIndicatorView;
    String phone;

    @Override
public void onBackPressed() {

         // Simply Do noting!
        }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mobile_enter);

        getSupportActionBar().hide();

        if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        }

        mobile=findViewById(R.id.mobileEnter_edtxt);

        btn=findViewById(R.id.mobileEnter_nxtbtn);
        avLoadingIndicatorView=findViewById(R.id.avi);

        viewPager=findViewById(R.id.mobile_vpager);

        mobileEnterSliderAdapter=new MobileEnterSliderAdapter(MobileEnter.this);
        viewPager.setAdapter(mobileEnterSliderAdapter);




         update= new Runnable() {
            public void run() {
                if (currentPage == NUM_PAGES - 1) {
                    currentPage = 0;
                }
                else
                    currentPage++;
                viewPager.setCurrentItem(currentPage, true);
            }
        };

        new Timer().schedule(new TimerTask() {

            @Override
            public void run() {
                handler.post(update);
            }
        }, 1000, 5000);


        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                phone= mobile.getText().toString().trim();
                if(phone.length()==10)
                {
                    if (send(phone))
                    {
                        btn.setVisibility(View.GONE);
                        avLoadingIndicatorView.smoothToShow();

                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                startActivity(new Intent(MobileEnter.this, Otp.class).putExtra("phone", phone)
                                        .putExtra("sliderItem", viewPager.getCurrentItem())
                                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK));
                            }
                        }, 2000);
                    }
                    else  Toast.makeText(getApplicationContext(),"Could'nt send OTP to this number.\nPlease try again",Toast.LENGTH_SHORT).show();


                }
                else Toast.makeText(getApplicationContext(),"Invalid number",Toast.LENGTH_SHORT).show();
            }
        });

    }

boolean send(String phone)
{


    return  true;

}

}
