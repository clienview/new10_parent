package com.atotz.abhay.new10_parent;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.atotz.abhay.new10_parent.adapters.MobileEnterSliderAdapter;
import com.atotz.abhay.new10_parent.models.RegisterModel;
import com.atotz.abhay.new10_parent.models.SharedStorage.SharedStorage;
import com.atotz.abhay.new10_parent.network.Network;
import com.google.gson.Gson;
import com.wang.avi.AVLoadingIndicatorView;
import com.yarolegovich.lovelydialog.LovelyStandardDialog;

import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Otp extends AppCompatActivity {
    Button back;
    CardView btn;
    MobileEnterSliderAdapter mobileEnterSliderAdapter;
    ViewPager viewPager;
    int currentPage = 0, NUM_PAGES = 3;
    Handler handler = new Handler();
    Runnable update;
    Bundle extras;
    RegisterModel registerModel = new RegisterModel();
    Dialog lovelyStandardDialog;
    CountDownTimer countDownTimer;
    AVLoadingIndicatorView avLoadingIndicatorView;
    TextView timer;
    EditText otp;
    TelephonyManager mngr;
    String imei;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);

        getSupportActionBar().hide();

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        }

        btn = findViewById(R.id.otp_gobtn);
        back = findViewById(R.id.otp_re_enter_btn);
        viewPager = findViewById(R.id.otp_vpager);

        avLoadingIndicatorView = findViewById(R.id.avi);
        timer = findViewById(R.id.otp_timer_txt);
        otp = findViewById(R.id.otp_edtxt);

        mngr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);


        extras = getIntent().getExtras();
        if (extras != null) {
            currentPage = extras.getInt("sliderItem");
            Log.e("Item", String.valueOf(currentPage));

            viewPager.setCurrentItem(currentPage);

        }
        mobileEnterSliderAdapter = new MobileEnterSliderAdapter(Otp.this);
        viewPager.setAdapter(mobileEnterSliderAdapter);

        update = new Runnable() {
            public void run() {
                if (currentPage == NUM_PAGES - 1) {
                    currentPage = 0;
                } else
                    currentPage++;
                viewPager.setCurrentItem(currentPage, true);
            }
        };
        new Timer().schedule(new TimerTask() {

            @Override
            public void run() {
                handler.post(update);
            }
        }, 1000, 5000);

        countDownTimer = new CountDownTimer(3 * 60 * 1000, 1000) {
            @Override
            public void onTick(long l) {
                timer.setText((l / 60000) + ":" + (l % 60000 / 1000));
            }

            @Override
            public void onFinish() {

                if (!Otp.this.isFinishing()) {
                    showTimerDialog(Otp.this);
                }

            }
        };
        countDownTimer.start();

        otp.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.equals(SharedStorage.OTPRecieved)) {
                    register(getIntent().getStringExtra("phone"));
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String typed = otp.getText().toString().trim();

                if (typed.equals(SharedStorage.OTPRecieved)) {
                    register(getIntent().getStringExtra("phone"));
                } else {
                    Toast.makeText(getApplicationContext(), "Invalid OTP", Toast.LENGTH_SHORT).show();
                }

            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), MobileEnter.class)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                finish();
            }
        });
    }


    void showTimerDialog(Context context) {

        lovelyStandardDialog = new LovelyStandardDialog(context, LovelyStandardDialog.ButtonLayout.HORIZONTAL)
                .setTopColorRes(R.color.Green2)
                .setButtonsColorRes(R.color.Green2)
                .setIcon(R.mipmap.ic_launcher_round)
                .setTitle("New10")
                .setMessage("You haven't entered OTP yet.want some more time?")
                .setPositiveButton("Yes", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        lovelyStandardDialog.dismiss();
                        countDownTimer.cancel();
                        countDownTimer.start();
                    }
                })
                .setNegativeButton("No", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finish();

                    }
                })
                .show();


    }

    void register(String phone) {
        btn.setVisibility(View.GONE);
        timer.setVisibility(View.GONE);
        avLoadingIndicatorView.smoothToShow();
        back.setVisibility(View.GONE);

        getIMEI();
        Call<RegisterModel> call = Network.getInstance().registerParent(phone);
        call.enqueue(new Callback<RegisterModel>() {
            @Override
            public void onResponse(Call<RegisterModel> call, Response<RegisterModel> response) {
                registerModel = response.body();


                Log.e("Registration", new Gson().toJson(registerModel));

                String status = registerModel.getStatus();
                String type = registerModel.getType();

                if (type.equals("parent") && status.equals("insert") || status.equals("update")) {
                    SharedStorage.userId = String.valueOf(registerModel.getId());

                    startActivity(new Intent(getApplicationContext(), ParentHome.class));

                }
            }

            @Override
            public void onFailure(Call<RegisterModel> call, Throwable t) {
                Log.e("error", t.getMessage());
Toast.makeText(getApplicationContext(),"Unable to register now..please try again!",Toast.LENGTH_SHORT).show();
            }
        });
    }

    void getIMEI() {


        if (ContextCompat.checkSelfPermission(Otp.this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(Otp.this,
                    Manifest.permission.READ_PHONE_STATE)) {
            } else {
                String[] perms = {"android.permission.READ_PHONE_STATE"};
                ActivityCompat.requestPermissions(Otp.this, perms, 101);
            }
        } else {
            // Permission has already been granted
            imei = mngr.getDeviceId();
            Log.e("imei", imei);

        }
    }

    @Override
    public void onRequestPermissionsResult(int permsRequestCode, String[] permissions, int[] grantResults) {

        switch (permsRequestCode) {

            case 101:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.e("as", "Permission granted");
                }
                break;

        }}}