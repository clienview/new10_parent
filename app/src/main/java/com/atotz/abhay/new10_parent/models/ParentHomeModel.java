package com.atotz.abhay.new10_parent.models;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ParentHomeModel {

    public ParentHomeModel(String pkIntUsersId, String vchrUsersUsername, String vchrUsersPassword, String vchrUsersEmail, String vchrUsersName, String vchrUsersPhone, String fkIntAttributeIdMedium, String fkIntAttributeIdBoard, String fkIntAttributeIdClass, String txtUsersAccountType, String fkIntAttributeIdGallery, String txtUsersRemaining, String txtUsersStage, String txtUsersImei) {
        this.pkIntUsersId = pkIntUsersId;
        this.vchrUsersUsername = vchrUsersUsername;
        this.vchrUsersPassword = vchrUsersPassword;
        this.vchrUsersEmail = vchrUsersEmail;
        this.vchrUsersName = vchrUsersName;
        this.vchrUsersPhone = vchrUsersPhone;
        this.fkIntAttributeIdMedium = fkIntAttributeIdMedium;
        this.fkIntAttributeIdBoard = fkIntAttributeIdBoard;
        this.fkIntAttributeIdClass = fkIntAttributeIdClass;
        this.txtUsersAccountType = txtUsersAccountType;
        this.fkIntAttributeIdGallery = fkIntAttributeIdGallery;
        this.txtUsersRemaining = txtUsersRemaining;
        this.txtUsersStage = txtUsersStage;
        this.txtUsersImei = txtUsersImei;
    }

    @SerializedName("pk_int_users_id")
    @Expose
    private String pkIntUsersId;
    @SerializedName("vchr_users_username")
    @Expose
    private String vchrUsersUsername;
    @SerializedName("vchr_users_password")
    @Expose
    private String vchrUsersPassword;
    @SerializedName("vchr_users_email")
    @Expose
    private String vchrUsersEmail;
    @SerializedName("vchr_users_name")
    @Expose
    private String vchrUsersName;
    @SerializedName("vchr_users_phone")
    @Expose
    private String vchrUsersPhone;
    @SerializedName("fk_int_attribute_id_medium")
    @Expose
    private String fkIntAttributeIdMedium;
    @SerializedName("fk_int_attribute_id_board")
    @Expose
    private String fkIntAttributeIdBoard;
    @SerializedName("fk_int_attribute_id_class")
    @Expose
    private String fkIntAttributeIdClass;
    @SerializedName("txt_users_account_type")
    @Expose
    private String txtUsersAccountType;
    @SerializedName("fk_int_attribute_id_gallery")
    @Expose
    private String fkIntAttributeIdGallery;
    @SerializedName("txt_users_remaining")
    @Expose
    private String txtUsersRemaining;
    @SerializedName("txt_users_stage")
    @Expose
    private String txtUsersStage;
    @SerializedName("txt_users_imei")
    @Expose
    private String txtUsersImei;

    public String getPkIntUsersId() {
        return pkIntUsersId;
    }

    public void setPkIntUsersId(String pkIntUsersId) {
        this.pkIntUsersId = pkIntUsersId;
    }

    public String getVchrUsersUsername() {
        return vchrUsersUsername;
    }

    public void setVchrUsersUsername(String vchrUsersUsername) {
        this.vchrUsersUsername = vchrUsersUsername;
    }

    public String getVchrUsersPassword() {
        return vchrUsersPassword;
    }

    public void setVchrUsersPassword(String vchrUsersPassword) {
        this.vchrUsersPassword = vchrUsersPassword;
    }

    public String getVchrUsersEmail() {
        return vchrUsersEmail;
    }

    public void setVchrUsersEmail(String vchrUsersEmail) {
        this.vchrUsersEmail = vchrUsersEmail;
    }

    public String getVchrUsersName() {
        return vchrUsersName;
    }

    public void setVchrUsersName(String vchrUsersName) {
        this.vchrUsersName = vchrUsersName;
    }

    public String getVchrUsersPhone() {
        return vchrUsersPhone;
    }

    public void setVchrUsersPhone(String vchrUsersPhone) {
        this.vchrUsersPhone = vchrUsersPhone;
    }

    public String getFkIntAttributeIdMedium() {
        return fkIntAttributeIdMedium;
    }

    public void setFkIntAttributeIdMedium(String fkIntAttributeIdMedium) {
        this.fkIntAttributeIdMedium = fkIntAttributeIdMedium;
    }

    public String getFkIntAttributeIdBoard() {
        return fkIntAttributeIdBoard;
    }

    public void setFkIntAttributeIdBoard(String fkIntAttributeIdBoard) {
        this.fkIntAttributeIdBoard = fkIntAttributeIdBoard;
    }

    public String getFkIntAttributeIdClass() {
        return fkIntAttributeIdClass;
    }

    public void setFkIntAttributeIdClass(String fkIntAttributeIdClass) {
        this.fkIntAttributeIdClass = fkIntAttributeIdClass;
    }

    public String getTxtUsersAccountType() {
        return txtUsersAccountType;
    }

    public void setTxtUsersAccountType(String txtUsersAccountType) {
        this.txtUsersAccountType = txtUsersAccountType;
    }

    public String getFkIntAttributeIdGallery() {
        return fkIntAttributeIdGallery;
    }

    public void setFkIntAttributeIdGallery(String fkIntAttributeIdGallery) {
        this.fkIntAttributeIdGallery = fkIntAttributeIdGallery;
    }

    public String getTxtUsersRemaining() {
        return txtUsersRemaining;
    }

    public void setTxtUsersRemaining(String txtUsersRemaining) {
        this.txtUsersRemaining = txtUsersRemaining;
    }

    public String getTxtUsersStage() {
        return txtUsersStage;
    }

    public void setTxtUsersStage(String txtUsersStage) {
        this.txtUsersStage = txtUsersStage;
    }

    public String getTxtUsersImei() {
        return txtUsersImei;
    }

    public void setTxtUsersImei(String txtUsersImei) {
        this.txtUsersImei = txtUsersImei;
    }

}