package com.atotz.abhay.new10_parent.interfaces;

public interface ParentChildClickListener {
    void onChildclick(int position,String image);
}
