package com.atotz.abhay.new10_parent;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.WindowManager;
import android.widget.ListView;
import com.atotz.abhay.new10_parent.adapters.UsageAdapter;
import com.atotz.abhay.new10_parent.models.ParentChildUsageModel;
import com.atotz.abhay.new10_parent.network.Network;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ParentChildUsage extends AppCompatActivity {
ListView listview;
List<ParentChildUsageModel> usagemodels=new ArrayList<>();
    UsageAdapter usageAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parent_child_usage);

        getSupportActionBar().hide();
        if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        }
        listview=findViewById(R.id.usage_lview);


        Bundle extras=getIntent().getExtras();
        if(extras!=null)
        {
            Call<List<ParentChildUsageModel>> call= Network.getInstance().parentChild30Usage(extras.getString("cid"));
            call.enqueue(new Callback<List<ParentChildUsageModel>>() {
                @Override
                public void onResponse(Call<List<ParentChildUsageModel>> call, Response<List<ParentChildUsageModel>> response) {
                    try {
                        usagemodels = response.body();
                    }catch (Exception e){}
                    Log.e("usages",new Gson().toJson(usagemodels));
                    usageAdapter=new UsageAdapter(usagemodels,ParentChildUsage.this);
                    listview.setAdapter(usageAdapter);
                  //  usageAdapter.notifyDataSetChanged();
                }

                @Override
                public void onFailure(Call<List<ParentChildUsageModel>> call, Throwable t) {
                    Log.e("usages",t.getMessage());
                }
            });
        }
    }
}
