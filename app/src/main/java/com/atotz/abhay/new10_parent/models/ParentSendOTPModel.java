package com.atotz.abhay.new10_parent.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ParentSendOTPModel {

    @SerializedName("otp")
    @Expose
    private String otp;
    @SerializedName("child_id")
    @Expose
    private String childId;
    @SerializedName("msg")
    @Expose
    private String msg;

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getChildId() {
        return childId;
    }

    public void setChildId(String childId) {
        this.childId = childId;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }


}



