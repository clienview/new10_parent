package com.atotz.abhay.new10_parent.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.atotz.abhay.new10_parent.R;
import com.atotz.abhay.new10_parent.interfaces.ParentChildClickListener;
import com.atotz.abhay.new10_parent.models.ChildList;
import com.atotz.abhay.new10_parent.models.ParentHomeModel;

import java.util.ArrayList;
import java.util.List;

public class ParentHomeChildAdapter extends RecyclerView.Adapter<ParentHomeChildAdapter.ViewHolder> {
    ParentChildClickListener parentChildClickListener;

    List<ParentHomeModel> childLists =new ArrayList<>();
    Context ctx;

    public ParentHomeChildAdapter(List<ParentHomeModel> childLists, Context ctx,ParentChildClickListener parentChildClickListener) {
        this.childLists = childLists;
        this.ctx = ctx;
        this.parentChildClickListener=parentChildClickListener;

    }

    @Override
    public ParentHomeChildAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.child_item, parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ParentHomeChildAdapter.ViewHolder holder, int position) {
        if(position==0){

           holder.name.setText("Add Child");
           holder.img.setImageResource(R.drawable.add);
        }
        else
            {
            holder.name.setText(childLists.get(position).getVchrUsersName());
            String name = childLists.get(position).getFkIntAttributeIdGallery();

try {
    int id = ctx.getResources().getIdentifier(name, "drawable", ctx.getPackageName());
    holder.img.setImageResource(id);
}catch (Exception e){e.getCause();}
        }
    }

    @Override
    public int getItemCount() {
        return childLists.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        ImageView img;


        public ViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.child_item_name_tview);
            img=itemView.findViewById(R.id.child_item_pic);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
parentChildClickListener.onChildclick(getAdapterPosition(),childLists.get(getAdapterPosition()).getFkIntAttributeIdGallery());
                }
            });
        }
    }
}
