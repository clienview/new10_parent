package com.atotz.abhay.new10_parent.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.atotz.abhay.new10_parent.R;
import com.atotz.abhay.new10_parent.interfaces.SubjectClickListener;
import com.atotz.abhay.new10_parent.models.SubjectModel;


import java.util.List;


public class ChildDetailsSubjectAdapter extends RecyclerView.Adapter<ChildDetailsSubjectAdapter.ViewHolder> {
    List<SubjectModel> subjectModels;
    Context ctx;
    SubjectClickListener subjectClickListener;
    int selectedPosition=0;

    public ChildDetailsSubjectAdapter(List<SubjectModel> subjectModels, Context ctx,SubjectClickListener subjectClickListener) {
        this.subjectModels = subjectModels;
        this.ctx = ctx;
        this.subjectClickListener=subjectClickListener;
    }

    @Override
    public ChildDetailsSubjectAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater= (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v=inflater.inflate(R.layout.subject_item,parent,false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ChildDetailsSubjectAdapter.ViewHolder holder, int position) {

        holder.name.setText(subjectModels.get(position).getName());
        if(selectedPosition==position)
        {
            holder.name.setBackground(ctx.getResources().getDrawable(R.drawable.child_subject_bg));
            holder.name.setTextColor(ctx.getResources().getColor(R.color.White));
        }
        else
        {
            holder.name.setBackgroundColor(ctx.getResources().getColor(R.color.Transperent));
            holder.name.setTextColor(ctx.getResources().getColor(R.color.Black));
        }

        //String img = subjectModels.get(position).getTxtGalleryName();
        //Glide.with(ctx).load(Config.GLIDE_IMAGE_BASE_URL+img).into(holder.pic);
    }

    @Override
    public int getItemCount() {
        return subjectModels.size();
    }
    class ViewHolder extends RecyclerView.ViewHolder{
        Button name;
        //ImageView pic;

        public ViewHolder(View itemView) {
            super(itemView);

            name=itemView.findViewById(R.id.subject_item_subject_btn);
            //pic=itemView.findViewById(R.id.subject_item_subject_pic)
            name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    selectedPosition=getAdapterPosition();
                    subjectClickListener.onSubjectClick(getAdapterPosition());
                    notifyDataSetChanged();
                }
            });

        }
    }



























}
