package com.atotz.abhay.new10_parent;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.atotz.abhay.new10_parent.adapters.IntroAdapter;

public class Intro extends AppCompatActivity {
    Button skip,next;
    private LinearLayout dotsLayout;
    IntroAdapter introAdapter;
    ViewPager viewPager;

    TextView[] dots;
    int[] layouts={R.layout.intro_1,R.layout.intro_2,R.layout.intro_3};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_intro);


        if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        }

        next=findViewById(R.id.btn_next);
        skip=findViewById(R.id.btn_skip);

        viewPager=findViewById(R.id.intro_Vpager);

        dotsLayout=findViewById(R.id.dots_layout);

        introAdapter=new IntroAdapter(Intro.this);

        viewPager.setAdapter(introAdapter);

        getSupportActionBar().hide();
//        prepareDots(0);

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(viewPager.getCurrentItem()!=2)
                {
                    viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
                }
                else startActivity(new Intent(getApplicationContext(),ParentHome.class)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK));

            }
        });
        next.setText("NEXT");
        skip.setVisibility(View.VISIBLE);
        next.setVisibility(View.VISIBLE);

        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),ParentHome.class)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK));
            }
        });

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(final int position) {
                prepareDots(position);
if(position==2){
    next.setText("START");
    next.setVisibility(View.VISIBLE);
    skip.setVisibility(View.GONE);

}
else
{
    next.setText("NEXT");
    skip.setVisibility(View.VISIBLE);
    next.setVisibility(View.VISIBLE);
}

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }
    void prepareDots(int position)
    {

if(dotsLayout!=null) {
    dotsLayout.removeAllViews();
    dots = new TextView[layouts.length];
    for (int i = 0; i < layouts.length; i++) {
        dots[i] = new TextView(this);
        dots[i].setText(Html.fromHtml("&#8226;"));
        dots[i].setTextSize(35);
        dots[i].setTextColor(getResources().getColor(R.color.dot_grey));
        dotsLayout.addView(dots[i]);
    }
    if (dots.length > 1) {
        dots[position].setTextColor(getResources().getColor(R.color.dot_white));

    }


}
    }


}
