package com.atotz.abhay.new10_parent;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.WindowManager;

import com.amitshekhar.DebugDB;
import com.atotz.abhay.new10_parent.models.SharedStorage.SharedStorage;

public class SplashPage extends AppCompatActivity {
Handler handler=new Handler();
Intent intent;
SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_page);
        getSupportActionBar().hide();

        if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        }


        intent=new Intent();
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);

        preferences=getSharedPreferences ("new10_parent", MODE_PRIVATE);
        Boolean firstRun=preferences.getBoolean("firstRun",true);

        Log.e("firstRun",firstRun.toString());
        Log.e("db", DebugDB.getAddressLog());


        if(firstRun)
        {
            intent.setClass(SplashPage.this,Intro.class);

            SharedPreferences.Editor editor=preferences.edit();
            editor.putBoolean("firstRun",false);
            editor.apply();
        }
        else {

            intent.setClass(SplashPage.this,ParentHome.class);

        }

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(intent);
            }
        },2500);



    }
}
