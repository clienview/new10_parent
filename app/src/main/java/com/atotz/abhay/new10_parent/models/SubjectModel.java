package com.atotz.abhay.new10_parent.models;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SubjectModel {
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("total_mark")
    @Expose
    private String totalMark;
    @SerializedName("answer_mark")
    @Expose
    private String answerMark;

    @SerializedName("usage")
    @Expose
    private List<SubjectUsage> usage = null;

    public List<SubjectUsage> getUsage() {
        return usage;
    }

    public void setUsage(List<SubjectUsage> usage) {
        this.usage = usage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTotalMark() {
        return totalMark;
    }

    public void setTotalMark(String totalMark) {
        this.totalMark = totalMark;
    }

    public String getAnswerMark() {
        return answerMark;
    }

    public void setAnswerMark(String answerMark) {
        this.answerMark = answerMark;
    }

}
