package com.atotz.abhay.new10_parent.adapters;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.atotz.abhay.new10_parent.R;

public class IntroAdapter extends PagerAdapter {
    Context context;
    LayoutInflater inflater;

int[] layouts={R.layout.intro_1,R.layout.intro_2,R.layout.intro_3};
    public IntroAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        return layouts.length;

    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view==object;

    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v=inflater.inflate(layouts[position],null);
        container.addView(v);
        return v;

    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
       container.removeView((View)object);
    }
}
