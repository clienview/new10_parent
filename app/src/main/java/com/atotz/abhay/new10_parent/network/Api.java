package com.atotz.abhay.new10_parent.network;

import com.atotz.abhay.new10_parent.models.ParentChildAddModel;
import com.atotz.abhay.new10_parent.models.ParentChildDetailModel;
import com.atotz.abhay.new10_parent.models.ParentChildUsageModel;
import com.atotz.abhay.new10_parent.models.ParentHomeModel;
import com.atotz.abhay.new10_parent.models.ParentSendOTPModel;
import com.atotz.abhay.new10_parent.models.RegisterModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface Api {
    @FormUrlEncoded
    @POST("register_parent")
    Call<RegisterModel> registerParent(@Field("phone_number") String phoneNumber);


    @FormUrlEncoded
    @POST("child_list")
    Call<List<ParentHomeModel>> getParentHome(@Field("child_ids") String child_ids);


    @FormUrlEncoded
    @POST("child_hour")
    Call<ParentChildDetailModel> getChildDetails(@Field("child_id") String child_id);



    @FormUrlEncoded
    @POST("parent_child_add")
    Call<ParentSendOTPModel> parentSendOTP(@Field("mobile_number") String mob);



    @FormUrlEncoded
    @POST("child_30_usage")
    Call<List<ParentChildUsageModel>> parentChild30Usage(@Field("child_id") String childId);

}
