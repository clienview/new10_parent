package com.atotz.abhay.new10_parent.network;

import com.atotz.abhay.new10_parent.BuildConfig;
import com.atotz.abhay.new10_parent.config.Config;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Network {

static  Retrofit retrofit;
static Api api;



public static Api getInstance()           //creating api interface
{
    if(retrofit==null)
    {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder  builder= new OkHttpClient.Builder();
        if(BuildConfig.DEBUG)
        {
            builder.addInterceptor(interceptor);
        }

        OkHttpClient client=builder.build();

        GsonBuilder gsonBuilder = new GsonBuilder().setLenient();
        Gson gson = gsonBuilder.create();


        retrofit=new Retrofit
            .Builder()
            .baseUrl(Config.RETROFIT_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
            .build();
    api=retrofit.create(Api.class);
    }
    return api;

}


}
