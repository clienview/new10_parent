package com.atotz.abhay.new10_parent;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.atotz.abhay.new10_parent.adapters.ChildDetailsSubjectAdapter;
import com.atotz.abhay.new10_parent.interfaces.SubjectClickListener;
import com.atotz.abhay.new10_parent.models.ParentChildDetailModel;
import com.atotz.abhay.new10_parent.models.SubjectUsage;
import com.atotz.abhay.new10_parent.network.Network;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;

import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.utils.EntryXComparator;
import com.google.gson.Gson;
import com.google.gson.JsonParseException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ParentChildDetails extends AppCompatActivity{


    RecyclerView rview;
    ParentChildDetailModel parentChildDetailModel=new ParentChildDetailModel();
    TextView userName,id,name,last,total;
    CircleImageView userImg;

    ChildDetailsSubjectAdapter  childDetailsSubjectAdapter;
    LineChart chart;
    LineChart chart2;
    TextView score;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parent_child_details);

        if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        }


        setTitle("Child Details");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        chart= findViewById(R.id.barchart);
        chart2= findViewById(R.id.chart2);
        userName=findViewById(R.id.profile_layout_name_txt);
        userImg=findViewById(R.id.profile_layout_image);
        id=findViewById(R.id.childDetail_id);
        name=findViewById(R.id.childDetail_name);
        total=findViewById(R.id.childDetail_total);
        last=findViewById(R.id.childDetail_last);
        userImg.setImageResource(R.drawable.img_7);
        rview=findViewById(R.id.child_detail_subject_rview);
        score=findViewById(R.id.score_tview);



        final Bundle extras=getIntent().getExtras();
        if(extras!=null) {

            id.setText(extras.getString("cid"));
            name.setText(extras.getString("cname"));
            try {
                String image=getIntent().getStringExtra("image");
                int id = getResources().getIdentifier(image, "drawable",getPackageName());
                userImg.setImageResource(id);
            }catch (Exception e){e.getCause();}



            Call<ParentChildDetailModel> call = Network.getInstance().getChildDetails(extras.getString("cid"));
            call.enqueue(new Callback<ParentChildDetailModel>() {
                @Override
                public void onResponse(Call<ParentChildDetailModel> call, Response<ParentChildDetailModel> response) {

                    try {
                        parentChildDetailModel = response.body();
                    }catch (JsonParseException e){
                        Log.e("Error", e.getMessage());
                    }

                    Log.e("ChildDetails", new Gson().toJson(parentChildDetailModel));





                    total.setText(parentChildDetailModel.getTotal()+" Minutes");
                    last.setText(parentChildDetailModel.getLast()+" Minutes");

                    setUpSubjectWiseGraph(0);

                    rview.setLayoutManager(new LinearLayoutManager(ParentChildDetails.this,LinearLayoutManager.HORIZONTAL,false));

                    childDetailsSubjectAdapter = new ChildDetailsSubjectAdapter(parentChildDetailModel.getSubjectModels(), ParentChildDetails.this, new SubjectClickListener() {
                        @Override
                        public void onSubjectClick(int position) {
                            setUpSubjectWiseGraph(position);
                        }
                    });
                    rview.setAdapter(childDetailsSubjectAdapter);
                    setUpUsageGraph(extras);
                }

                @Override
                public void onFailure(Call<ParentChildDetailModel> call, Throwable t) {
Log.e("Error",t.getMessage());
                }
            });
            }
  }

  void setUpSubjectWiseGraph(int postion)
  {
      List<Entry> entries = new ArrayList<>();

      List<SubjectUsage> subjectModels=parentChildDetailModel.getSubjectModels().get(postion).getUsage();
      for (int i = 0; i<subjectModels.size(); i++) {

          String date=subjectModels.get(i).getDate();
          String[] dates=date.split("-");

          entries.add(new Entry(Float.parseFloat(dates[2]),
                  Float.parseFloat(subjectModels.get(i).getCount())));
      }
      Collections.sort(entries, new EntryXComparator());


      LineDataSet dataSet = new LineDataSet(entries,""); // add entries to dataset
      dataSet.setColor(getResources().getColor(R.color.Green2));
      dataSet.setValueTextColor(R.color.Black); // styling, ...
      dataSet.setDrawValues(false);
      dataSet.setDrawCircles(false);
      dataSet.setMode(LineDataSet.Mode.HORIZONTAL_BEZIER);
      dataSet.setCubicIntensity(0.2f);
      dataSet.setLineWidth(2);


      XAxis xAxis = chart2.getXAxis();
      xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
      xAxis.setTextSize(10f);
      xAxis.setDrawAxisLine(true);
      xAxis.setDrawGridLines(false);

      YAxis leftAxis = chart2.getAxisLeft();
      leftAxis.setDrawLabels(true); // no axis labels
      leftAxis.setDrawAxisLine(false); // no axis line
      leftAxis.setDrawGridLines(false); // no grid lines
      leftAxis.setDrawZeroLine(false); // draw a zero line


      YAxis rightAxis = chart2.getAxisRight();
      rightAxis.setDrawLabels(false); // no axis labels
      rightAxis.setDrawAxisLine(false); // no axis line
      rightAxis.setDrawGridLines(false); // no grid lines
      rightAxis.setDrawZeroLine(false); // draw a zero line
      // mChart.getAxisRight().setEnabled(false); // no right axis


      LineData lineData = new LineData(dataSet);
      chart2.setData(lineData);
      chart2.getLegend().setEnabled(false);
      chart2.invalidate(); // refresh


      score.setText("Score: "+parentChildDetailModel.getSubjectModels().get(postion).getAnswerMark()+
              "/"+parentChildDetailModel.getSubjectModels().get(postion).getTotalMark());
  }


  void setUpUsageGraph(final Bundle extras)
  {

      List<Entry> entries = new ArrayList<>();
      for (int i = 0; i<parentChildDetailModel.getUsageGraph().size(); i++) {

          String date=parentChildDetailModel.getUsageGraph().get(i).getDate();
          String[] dates=date.split("-");

          entries.add(new Entry(Float.parseFloat(dates[2]),
                  Float.parseFloat(parentChildDetailModel.getUsageGraph().get(i).getCount())));
      }
      Collections.sort(entries, new EntryXComparator());


      LineDataSet dataSet = new LineDataSet(entries,""); // add entries to dataset
      dataSet.setColor(getResources().getColor(R.color.Green2));
      dataSet.setValueTextColor(R.color.Black); // styling, ...
      dataSet.setDrawValues(false);
      dataSet.setDrawCircles(false);
      dataSet.setMode(LineDataSet.Mode.HORIZONTAL_BEZIER);
      dataSet.setCubicIntensity(0.2f);
      dataSet.setLineWidth(2);


      XAxis xAxis = chart.getXAxis();
      xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
      xAxis.setTextSize(10f);
      xAxis.setDrawAxisLine(true);
      xAxis.setDrawGridLines(false);

      YAxis leftAxis = chart.getAxisLeft();
      leftAxis.setDrawLabels(true); // no axis labels
      leftAxis.setDrawAxisLine(false); // no axis line
      leftAxis.setDrawGridLines(false); // no grid lines
      // leftAxis.setDrawZeroLine(true); // draw a zero line


      YAxis rightAxis = chart.getAxisRight();
      rightAxis.setDrawLabels(false); // no axis labels
      rightAxis.setDrawAxisLine(false); // no axis line
      rightAxis.setDrawGridLines(false); // no grid lines
      //rightAxis.setDrawZeroLine(true); // draw a zero line
      // mChart.getAxisRight().setEnabled(false); // no right axis


      LineData lineData = new LineData(dataSet);
      chart.setData(lineData);
      chart.getLegend().setEnabled(false);
      chart.invalidate(); // refresh

      chart.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View view) {
              startActivity(new Intent(getApplicationContext(),ParentChildUsage.class).putExtra("cid",extras.getString("cid")));
          }
      });



  }
    }
