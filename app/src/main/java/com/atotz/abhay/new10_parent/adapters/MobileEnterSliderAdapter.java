package com.atotz.abhay.new10_parent.adapters;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.atotz.abhay.new10_parent.R;


public class MobileEnterSliderAdapter extends PagerAdapter {
    Context context;
    LayoutInflater inflater;

    int[] imgs={R.drawable.mobile_1,R.drawable.mobile_2,R.drawable.mobile_3};
    public MobileEnterSliderAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        return imgs.length;

    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view==object;

    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v=inflater.inflate(R.layout.mobile_item,null);
        ImageView img=v.findViewById(R.id.mobile_img);
        img.setImageResource(imgs[position]);
        container.addView(v);
        return v;

    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View)object);
    }
}

