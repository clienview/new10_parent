package com.atotz.abhay.new10_parent.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ChildList {

    @SerializedName("vchr_users_name")
    @Expose
    private String vchrUsersName;
    @SerializedName("pk_int_users_id")
    @Expose
    private String pkIntUsersId;
    @SerializedName("fk_int_attribute_id_gallery")
    @Expose
    private String fkIntAttributeIdGallery;

    public ChildList(String vchrUsersName, String pkIntUsersId, String fkIntAttributeIdGallery) {
        this.vchrUsersName = vchrUsersName;
        this.pkIntUsersId = pkIntUsersId;
        this.fkIntAttributeIdGallery = fkIntAttributeIdGallery;
    }

    public String getVchrUsersName() {
        return vchrUsersName;
    }

    public void setVchrUsersName(String vchrUsersName) {
        this.vchrUsersName = vchrUsersName;
    }

    public String getPkIntUsersId() {
        return pkIntUsersId;
    }

    public void setPkIntUsersId(String pkIntUsersId) {
        this.pkIntUsersId = pkIntUsersId;
    }

    public String getFkIntAttributeIdGallery() {
        return fkIntAttributeIdGallery;
    }

    public void setFkIntAttributeIdGallery(String fkIntAttributeIdGallery) {
        this.fkIntAttributeIdGallery = fkIntAttributeIdGallery;
    }

}
