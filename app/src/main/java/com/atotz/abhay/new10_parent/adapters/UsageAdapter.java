package com.atotz.abhay.new10_parent.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


import com.atotz.abhay.new10_parent.R;
import com.atotz.abhay.new10_parent.models.ParentChildUsageModel;

import java.util.List;

public class UsageAdapter extends BaseAdapter {
  List<ParentChildUsageModel> parentChildUsageModels;
  Context ctx;

    public UsageAdapter(List<ParentChildUsageModel> parentChildUsageModels, Context ctx) {
        this.parentChildUsageModels = parentChildUsageModels;
        this.ctx=ctx;


    }

    @Override
    public int getCount() {
        return parentChildUsageModels.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater layoutInflater= (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v=layoutInflater.inflate(R.layout.usage_item,null);
        TextView date=v.findViewById(R.id.usage_item_date);
        TextView usage=v.findViewById(R.id.usagge_item_usage);

        date.setText(parentChildUsageModels.get(i).getDate());


        usage.setText(parentChildUsageModels.get(i).getCount()+" Minutes");
        return v;
    }
}
