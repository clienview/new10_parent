package com.atotz.abhay.new10_parent.interfaces;

public interface SubjectClickListener {
    void onSubjectClick(int position);

}
