package com.atotz.abhay.new10_parent.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ParentChildUsageModel {

    @SerializedName("count")
    @Expose
    private Double count;
    @SerializedName("date")
    @Expose
    private String date;

    public Double getCount() {
        return count;
    }

    public void setCount(Double count) {
        this.count = count;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

}
