package com.atotz.abhay.new10_parent.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ParentChildDetailModel {

    @SerializedName("total")
    @Expose
    private String total;
    @SerializedName("last")
    @Expose
    private String last;
    @SerializedName("usage_graph")
    @Expose
    private List<UsageGraph> usageGraph;

    @SerializedName("subject")
    @Expose
    private List<SubjectModel> subjectModels;

    public List<SubjectModel> getSubjectModels() {
        return subjectModels;
    }

    public void setSubjectModels(List<SubjectModel> subjectModels) {
        this.subjectModels = subjectModels;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getLast() {
        return last;
    }

    public void setLast(String last) {
        this.last = last;
    }

    public List<UsageGraph> getUsageGraph() {
        return usageGraph;
    }

    public void setUsageGraph(List<UsageGraph> usageGraph) {
        this.usageGraph = usageGraph;
    }

}
