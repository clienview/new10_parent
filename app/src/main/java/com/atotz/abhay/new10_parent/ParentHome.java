package com.atotz.abhay.new10_parent;

import android.app.Application;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import com.atotz.abhay.new10_parent.adapters.ParentHomeChildAdapter;
import com.atotz.abhay.new10_parent.interfaces.ParentChildClickListener;
import com.atotz.abhay.new10_parent.models.ChildList;
import com.atotz.abhay.new10_parent.models.ParentChildAddModel;
import com.atotz.abhay.new10_parent.models.ParentHomeModel;
import com.atotz.abhay.new10_parent.models.ParentSendOTPModel;
import com.atotz.abhay.new10_parent.models.SharedStorage.SharedStorage;
import com.atotz.abhay.new10_parent.network.Network;
import com.google.gson.Gson;
import com.google.gson.JsonParseException;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ParentHome extends AppCompatActivity {
RecyclerView childRview;
List<ParentHomeModel> childLists=new ArrayList<>();
List<ParentHomeModel> parentHomeModels=new ArrayList<>();
ParentHomeChildAdapter parentHomeChildAdapter;
AlertDialog dialog1,dialog2;
EditText editText,editText2;
ParentSendOTPModel parentSendOTPModel=new ParentSendOTPModel();
Button btnback;

TextView name;
TextView childrens;
List<String> childIds=new ArrayList<>();


   SQLiteDatabase db;

    CircleImageView img;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();

        if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        }


        childRview=findViewById(R.id.child_Rview);
        name=findViewById(R.id.profile_layout_name_txt);
        img=findViewById(R.id.profile_layout_image);
childrens=findViewById(R.id.heading_layout_text);

        img.setImageResource(R.drawable.img_7);


        db = openOrCreateDatabase("new10_parent",MODE_PRIVATE,null);
        db.execSQL("CREATE TABLE IF NOT EXISTS child_list(child_id VARCHAR);");

  //   db.execSQL("delete  from child_list;");
      // db.execSQL("insert into child_list values(?);",new String[]{"2"});
       // db.execSQL("insert into child_list values(?);",new String[]{"3"});



        loadData();

        childrens.setText("My Children");
        childRview.setLayoutManager(new GridLayoutManager(ParentHome.this, 2));
    }

    void showAddDialog()
    {
        AlertDialog.Builder builder=new AlertDialog.Builder(ParentHome.this);
        builder.setTitle("Add child");
        View v=getLayoutInflater().inflate(R.layout.parent_add_child1,null);
        editText=v.findViewById(R.id.mobileEnter_edtxt1);

        CardView cardView=v.findViewById(R.id.mobileEnt);
        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String mob=editText.getText().toString();
                if(!mob.equals("")&&mob.length()==10)
                {
                    sendOTP(mob);
                    dialog1.dismiss();
                }
                else
                {
                    Toast.makeText(getApplicationContext(),"Please enter a valid mobile number",Toast.LENGTH_SHORT).show();

                }
            }
        });
        builder.setView(v);
        builder.setCancelable(true);
        dialog1=builder.create();
        dialog1.show();
    }

    void showOTPDialog()
    {
        dialog1.dismiss();
        AlertDialog.Builder builder=new AlertDialog.Builder(ParentHome.this);
        builder.setTitle("Enter the OTP recieved in your child's phone");
        View v=getLayoutInflater().inflate(R.layout.parent_add_child2,null);
        editText2=v.findViewById(R.id.otp_edtxt);
        btnback=v.findViewById(R.id.otp_re_enter_btn);
        btnback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog2.dismiss();
                dialog1.show();
            }
        });

        editText2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                Log.e("entered",charSequence.toString());

            if(charSequence.toString().equals(SharedStorage.OTPRecieved))
            {
                Log.e("match","match");
                db.execSQL("insert into child_list values(?);", new String[]{parentSendOTPModel.getChildId()});
                dialog2.hide();
                loadData();
                 }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        builder.setView(v);
        builder.setCancelable(true);
        dialog2=builder.create();
        dialog2.show();
    }


    void loadData()
    {

        childLists.clear();
        childLists.add(new ParentHomeModel("",
                "",
                "",
                "",
                "",
                "",
                "",
                "","",
                "",
                "",
                "","",
                ""));

        parentHomeChildAdapter=new ParentHomeChildAdapter(childLists, ParentHome.this, new ParentChildClickListener() {
            @Override
            public void onChildclick(int position,String image) {
                if(position==0)
                {
                    showAddDialog();

                }
            }
        });
        childRview.setAdapter(parentHomeChildAdapter);

        Cursor resultSet = db.rawQuery("Select child_id from child_list",null);
        resultSet.moveToFirst();
        childIds.clear();
        for (int i = 0; i <resultSet.getCount() ; i++) {
            childIds.add(resultSet.getString(0));
            resultSet.moveToNext();
        }
        final String childIdsSting=new Gson().toJson(childIds);

        Log.e("childs",childIdsSting);

        Call<List<ParentHomeModel>> call= Network.getInstance().getParentHome(childIdsSting);
                call.enqueue(new Callback<List<ParentHomeModel>>() {
                    @Override
                    public void onResponse(Call<List<ParentHomeModel>> call, Response<List<ParentHomeModel>> response) {
                  try {
                    parentHomeModels=response.body();}
                    catch (JsonParseException  ex)
                    {}

                        Log.e("response",new Gson().toJson(parentHomeModels));

                        //SharedStorage.userName=parentHomeModel.getName();

                        //name.setText(SharedStorage.userName);
                        childLists.clear();
                        childLists.add(new ParentHomeModel("",
                                "",
                                "",
                                "",
                                "",
                                "",
                                "",
                                "","",
                                "",
                                "",
                                "","",
                                ""));

                        for (int i = 0; i <parentHomeModels.size() ; i++) {
                            childLists.add(parentHomeModels.get(i));

                        }

                        parentHomeChildAdapter=new ParentHomeChildAdapter(childLists, ParentHome.this, new ParentChildClickListener() {
                            @Override
                            public void onChildclick(int position,String image) {
                                if(position==0)
                                {
                                    showAddDialog();

                                }
                                else
                                {
                                    startActivity(new Intent(ParentHome.this,ParentChildDetails.class)
                                            .putExtra("cid",childLists.get(position).getPkIntUsersId())
                                    .putExtra("cname",childLists.get(position).getVchrUsersName())
                                    .putExtra("image",childLists.get(position).getFkIntAttributeIdGallery()));
                                }
                            }
                        });
                        childRview.setAdapter(parentHomeChildAdapter);


                    }

                    @Override
                    public void onFailure(Call<List<ParentHomeModel>> call, Throwable t) {
                        Log.e("error",t.getMessage());
                    }
                });

    }

















    void sendOTP(String num)
    {
        Call<ParentSendOTPModel> call= Network.getInstance().parentSendOTP(num);
        call.enqueue(new Callback<ParentSendOTPModel>() {
            @Override
            public void onResponse(Call<ParentSendOTPModel> call, Response<ParentSendOTPModel> response) {
                parentSendOTPModel=response.body();
                Log.e("OtpModel",new Gson().toJson(parentSendOTPModel));

                switch(parentSendOTPModel.getMsg())
                {
                    case "fail_parent":
                        Toast.makeText(
                            getApplicationContext(),
                            "Sorry..a parent has already registered in this number"
                            ,Toast.LENGTH_SHORT).show();
                        break;
                    case "not_found":
                        Toast.makeText(
                                getApplicationContext(),
                                "No child has registered with this phone number..!"
                                ,Toast.LENGTH_SHORT).show();
                        break;
                    case "already":
                        Toast.makeText(getApplicationContext(),"Child already exists",Toast.LENGTH_SHORT).show();
                        break;

                    case "success":
                        SharedStorage.OTPRecieved=parentSendOTPModel.getOtp();
                        showOTPDialog();
                        break;
                }
            }

            @Override
            public void onFailure(Call<ParentSendOTPModel> call, Throwable t) {

            }
        });


    }
}
